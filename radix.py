from random import randint
import time

print("==== Radix Sort ====")


def get_random_points(num):
    """
    arr = []

    rnd_num = randint(1, 1000000)

    for _ in range(num):
        while rnd_num in arr:
            rnd_num = randint(1, 10000000)

        arr.append(rnd_num)

    return arr
    """
    return [randint(1, 99999999) for _ in range(num)]


def split_int(rev, num):
    a = [int(i) for i in str(num)]
    if rev:
        a.reverse()
    return a


def pad_ints(arr):
    max_len = len(str(arr[0]))
    for num in arr[1:]:
        if max_len < len(str(num)):
            max_len = len(str(num))

    return [(("0" * (max_len - len(str(i)))) + str(i)) for i in arr]


def rebuild_array(arr):
    a = []
    for num in arr:
        num.reverse()
        a.append(int("".join([str(i) for i in num])))

    return a


def do_radix(nums):
    copy_nums = [0 for _ in range(len(nums))]
    place_counts = [[] for i in range(10)]
    for place in range(len(nums[0])):
        for place_count in place_counts:
            place_count.clear()

        for idx, num in enumerate(nums):
            place_counts[num[place]].append(idx)

        curr_idx = 0

        for count in place_counts:
            for idx in count:
                try:
                    copy_nums[curr_idx] = nums[idx]
                except:
                    print(idx)
                    exit()
                curr_idx += 1

        nums = copy_nums

    return nums


random_nums = get_random_points(100000)
print("finished getting random numbers")
moded_nums = [split_int(True, i) for i in pad_ints(random_nums)]

start = time.time()
sorted_nums = do_radix(moded_nums)
end = time.time()
print(f"radix took: {end - start}")

start = time.time()
sorted(random_nums)
end = time.time()
print(f"python took: {end - start}")

# print(random_nums)

# print(rebuild_array(sorted_nums))
