from random import randint

print("==== K-D Tree ====")

MAX_DIM = 3

class kdtree:
    # node: int
    # left: kdtree
    # right: kdtree
    def __init__(self, depth, node = None, left = None, right = None):
       self.node = node
       self.left = left
       self.right = right
       self.depth = depth
       self.tabs = '\t' * depth

    def __str__(self):
        return f"""\n{self.tabs}Node:  {self.node}
{self.tabs}Left:  {self.left}
{self.tabs}Right: {self.right}"""

def get_random_points(num):
    return [[randint(1, 20) for x in range(MAX_DIM)] for y in range(num)]

"""
when node is not set, curr_kd is not the overall_kd
"""
def do_kd(arr, depth, curr_kd, overall_kd=None):
    if overall_kd == None:
        print("====== setting overall kd ======")
        overall_kd = curr_kd
    print("\nnew call to do_kd")
    print(f"depth: {curr_kd.depth}")
    if (curr_kd.depth == 1):
        print(f"restarting from top")
    print(f"Current arr:\n\t{arr}")
    # print(f"Overall_kd:\n{overall_kd}")
    print(f"Current curr_kd:\n{curr_kd}")

    if len(arr) == 0:
        print("arr length is 0")
        return curr_kd
    elif curr_kd.node == None:
        print("node is None")
        curr_kd.node = arr[0]
        do_kd(arr[1:], True, curr_kd, overall_kd)
    else:
        dim = depth % MAX_DIM
        print(f"currently looking at dimension: {dim}")
        if curr_kd.node[dim] > arr[0][dim]:
            print("node x is greater than arr[0] x")
            if curr_kd.left == None:
                print("left node is not set")
                curr_kd.left = kdtree(node = arr[0], depth = curr_kd.depth + 1) 
                do_kd(arr[1:], 0, overall_kd, overall_kd)
            else:
                do_kd(arr, depth + 1, curr_kd.left, overall_kd)
                # do_kd(arr[1:], True, overall_kd)
        else:
            print("node x is less than arr[0] x")
            if curr_kd.right == None:
                print("right node is not set")
                curr_kd.right = kdtree(node = arr[0], depth = curr_kd.depth + 1)
                do_kd(arr[1:], 0, overall_kd, overall_kd)
            else:
                do_kd(arr, depth + 1, curr_kd.right, overall_kd)
                # do_kd(arr[1:], True, overall_kd)

    return curr_kd

points = sorted(get_random_points(10))
print(points)
points = points[len(points) // 2:] + points[0: len(points) // 2]
print(points)
k = do_kd(points, False, kdtree(1))

print(points)
print(k)

