pub mod fileio {
    use std::env;
    use std::fs::File;
    use std::io::prelude::*;
    use std::process;

    pub fn read_file_from_argv() -> String {
        let args: Vec<String> = env::args().collect();

        if args.len() == 1 {
            println!("cargo run [input file]");
            process::exit(1);
        }

        let file_path: &String = &args[1];
        let mut file = match File::open(file_path) {
            Ok(f) => f,
            Err(_) => process::exit(1),
        };
        let mut contents = String::new();
        match file.read_to_string(&mut contents) {
            Ok(_) => println!("File loaded"),
            Err(_) => process::exit(1),
        };

        contents
    }
}
