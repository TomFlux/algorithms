fn get_character_at_pos(string: &String, pos: usize) -> String {
    string.chars().skip(pos).take(1).collect()
}

fn compare_two_chars(string: &String, cnd: usize, pos: usize) -> bool {
    get_character_at_pos(string, cnd) == get_character_at_pos(string, pos)
}

fn build_substring_table(search_string: &String) -> Vec<usize> {
    let mut table: Vec<usize> = vec![0];

    let mut cnd = 0;
    let mut pos = 1;

    while pos + 1 < search_string.len() {
        // find match step
        while !compare_two_chars(search_string, cnd, pos) && pos < search_string.len() {
            table.push(cnd);
            pos += 1
        }

        // find length of suffix/prefix step
        while compare_two_chars(search_string, cnd, pos) && pos < search_string.len() {
            table.push(cnd + 1);

            cnd += 1;
            pos += 1;
        }

        // roll back step
        while cnd != 0 && !compare_two_chars(search_string, cnd, pos) {
            cnd -= 1;
        }
    }

    table
}

fn search(haystack: &String, needle: &String, lps: &Vec<usize>) -> usize {
    let mut trgt = 0;
    let mut symb = 0;

    while trgt + 1 < haystack.len() {
        let mut trgt_char = get_character_at_pos(haystack, trgt);
        let mut symb_char = get_character_at_pos(needle, symb);

        // find match
        while trgt_char != symb_char {
            trgt += 1;
            trgt_char = get_character_at_pos(haystack, trgt);
        }

        // check for whole need
        while trgt_char == symb_char {
            if symb + 1 == needle.len() {
                return trgt - symb;
            }

            trgt += 1;
            symb += 1;

            trgt_char = get_character_at_pos(haystack, trgt);
            symb_char = get_character_at_pos(needle, symb);
        }

        if symb - lps[symb - 1] > 0 {
            symb -= lps[symb - 1]
        } else {
            symb = 0
        }
    }

    9999
}

fn main() {
    /*let haystack = String::from("dsgwadsxdsgwadsgz");
    let needle = String::from("dsgwadsgz");*/

    let haystack = String::from("BBAAAAABAAABA");
    let needle = String::from("AAAA");

    let backtrack_table: Vec<usize> = build_substring_table(&needle);
    println!("{:?}", backtrack_table);

    let res = search(&haystack, &needle, &backtrack_table);

    let before: String = haystack.chars().skip(0).take(res).collect();
    let after: String = haystack
        .chars()
        .skip(res + needle.len())
        .take(haystack.len() - (res + needle.len()))
        .collect();

    println!("{}\x1b[0;31m{}\x1b[0m{}", before, needle, after);
}
